import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SwingSum extends JFrame {
	
	public SwingSum() {
		FlowLayout flowlayout = new FlowLayout();
		JPanel panel = new JPanel(flowlayout);
		JLabel label = new JLabel("Enter numbers to find the sum:");
		JTextField num1 = new JTextField(10);
		JTextField num2 = new JTextField(10);
		JButton button = new JButton("Calculate");
		button.addActionListener(new ActionListener() {
			
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JOptionPane.showMessageDialog(null, "The total of the two numbers is");
		}
		});
		
		panel.add(label);
		panel.add(num1);
		panel.add(num2);
		panel.add(button);
		
		add(panel);
		setSize(200,200);
		setVisible(true);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
